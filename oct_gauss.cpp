/*!
   \file main.cpp
   \brief This file contains the main function of the program.

          In this main function I gather the initial input for the program.
          Here is also defined the function func which is the function that defines the system of diferential equation
          that will be solved by the Adams-Moulton metod, and the function set_initial_condition which creats an array
          for the initial values of the wave function parameters
   \author Alejandro Ramos
   \date 21/05/2019
*/


#include "../include/linear_systems.h"//Esta biblioteca no pertenece a C++
#include <functional>
#include <gsl/gsl_math.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>
#include <limits>


#define ps2au 41341.4		//Un picosegundo en unidades atómicas
#define fs2au 41.3414   //Un picosegundo en unidades atómicas
#define epsilon 0.000134135    //Epsilon en unidades atómicas
#define sigma 5.20242			//Sigma en unidades atómicas

# define PI          3.1415926535897932384626433 /*!< PI value taken from Wikipedia*/

#include "class_include.h"

using namespace std;
using namespace std::placeholders;

void write_monit_to_file(double *t, size_t N, double **monit, size_t n_monit, const char *file_name, const char *header) //Imprime una matriz
{
  FILE *data;

  data = fopen(file_name, "w");
  fprintf(data, "%s", header);

  for (int i=0; i < N; i++)
  {
    fprintf(data, "\n%e  ", t[i]);
    for (int j = 0; j < n_monit; j++)
    {
      fprintf(data, "%e  ", monit[i][j]);
    }
  }

  fclose(data);
}

void set_state(double *y, double alfa_0, double beta_0, double x0, double px0, size_t n_eq)
{

  for (size_t i = 0; i < 1; i++)
  {
    y[i + 0] = alfa_0; //Equation for alfa
    y[i + 1] = beta_0; //Equation for beta

    y[i + 2] = x0; //Equation for x0
    y[i + 3] = px0; //Equation for px0
  }
}

void take(double *y, double *y0, size_t i0, size_t i1)
{
  size_t size=(i1-i0);
  for (int i=0 ; i<size ; i++)
  {
    y[i]=y0[i0+i];
  }
}

void order_field(double *t_asc, double *e_asc, double *t, double *e_t, size_t N)
{
  //If time is already in ascending order, remains the same for the spline
  if (t[1] > t[0])
  {
    for (int j = 0; j < N ; j++)
    {
      t_asc[j] = t[j];
      e_asc[j] = e_t[j];
    }
  }
  //If time is NOT in ascending order, I invert it
  else if (t[1] < t[0])
  {
    for (int j = 0; j < N ; j++)
    {
      t_asc[j] = t[N -1 - j];
      e_asc[j] = e_t[N -1 - j];
    }
  }
  //This are the only 2 possibilities, so else I throw an error
  else
  {
    throw invalid_argument("!!!!!!!!!! Field must be in strictly ascending or descending order !!!!!!!!!!!!!!!!");
  }
}

/**
 * @brief This is a function that returns the differential equation class. It converts the field in strictly ascending order and creats the 
 *        electric field spline to pass to the differential equation
 * 
 * @param acc An accelerator pointer for the spline
 * @param efield_spline The spline pointer
 * @param q Charge
 * @param m Mass
 * @param vmax Maximum value of the barrier
 * @param a Distance from the barrier to the well 
 * @param im_re If it is imaginary or real propagation
 * @param t The pointer to the array of time instants. It has size N+1
 * @param e_t The pointer to the array with electrical field corresponding to time instant t/ It has size N+1
 * @param a0 The lectric field penalty
 * @param h The step to calculate the numerical derivative of the field. Must be small for the derivative to be accurate, but not to much that numerical errors start to appear 
 * @param N The number of points where the wavefunction is calculated. Icluding the initial point there are N+1.
 * @param T The lenght in time of the simulation. T=N*dt
 * @param file_name where I will output the spline of the field
 * @param wf If I'm performing a Psi propagation or a Chi propagation. This is important for the sign of the field.
 * @return dif_eq Returns the differential equation class.
 */
dif_eq construct_dif_eq(gsl_interp_accel *&acc, gsl_spline *&efield_spline, double q, double m, double vmax, double a, int im_re, double *t, double *e_t, double a0, size_t N, double T, int for_back)
{
  double* t_asc; //Ascending time
  double* e_asc; //Corresponding ascending time field

  //Allocating memory
  vector_double_allocation(t_asc,N+1);
  vector_double_allocation(e_asc,N+1);

  order_field(t_asc,e_asc,t,e_t,N+1);
  
  //Allocating the spline accelerator
  acc = gsl_interp_accel_alloc();                              //Alocating accelerator
  efield_spline = gsl_spline_alloc(gsl_interp_cspline, N + 1); //Alocating cubic spline

  // Create the spline with the field data
  gsl_spline_init(efield_spline, t_asc, e_asc, N + 1);

  // Defining the electrical field function
  std::function<double(double)> E = std::bind(gsl_spline_eval, efield_spline, _1, acc); 

  // Defining the diferential equation with the field
  dif_eq func(q, m, vmax, a, im_re, E, a0, T, for_back);

  // double *t_spline; //Ascending time
  // double *e_spline; //Corresponding ascending time field

  // //Allocating memory
  // vector_double_allocation(t_spline, 3 * N + 1);
  // vector_double_allocation(e_spline, 3 * N + 1);

  // for (int i=0; i<3*N+1;i++)
  // {
  //   t_spline[i]=i*T/(3*N);
  //   e_spline[i]=E(t_spline[i]);
  // }

  // //write_field_to_file(t_spline, e_spline, 3*N + 1, file_name, "# t (au)    e (Eh/(e a0))");

  //Deleting the allocated pointers
  delete[] e_asc;
  delete[] t_asc;

  return func;
}

void read_initial_data(double &q, double &m, double &vmax, double &a, double &E0, double &w, double &phi, double &a0, double &alfa_0, double &beta_0, double &x0, double &px0, double &alfa_f, double &beta_f, double &xf, double &pxf, double &ti, size_t &N, double &dt, size_t &step, int &im_re, int &iter, int &print)
{
  // Particle data
  cout << "Enter q: ";
  scanf("%lf %*s", &q);

  cout << "Enter m: ";
  scanf("%lf %*s", &m);

  // Potential data
  cout << "Enter vmax: ";
  scanf("%lf %*s", &vmax);

  cout << "Enter a: ";
  scanf("%lf %*s", &a);

  //Field parameters
  cout << "Enter E0: ";
  scanf("%lf %*s", &E0);

  cout << "Enter w: ";
  scanf("%lf %*s", &w);

  cout << "Enter phi: ";
  scanf("%lf %*s", &phi);

  cout << "Enter a0: ";
  scanf("%lf %*s", &a0);

  // Initial state data
  cout << "Enter alfa_0: ";
  scanf("%lf %*s", &alfa_0);

  cout << "Enter beta_0: ";
  scanf("%lf  %*s", &beta_0);

  cout << "Enter x0: ";
  scanf("%lf  %*s", &x0);

  cout << "Enter px0: ";
  scanf("%lf  %*s", &px0);

  // Final state data
  cout << "Enter alfa_f: ";
  scanf("%lf %*s", &alfa_f);

  cout << "Enter beta_f: ";
  scanf("%lf  %*s", &beta_f); /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  cout << "Enter xf: ";
  scanf("%lf  %*s", &xf);

  cout << "Enter pxf: ";
  scanf("%lf  %*s", &pxf);

  //Simulation data
  cout << "Enter ti(fs): ";
  scanf("%lf  %*s", &ti);
  ti = ti * fs2au;

  cout << "Enter the number of time steps: ";
  scanf("%zu  %*s", &N);

  cout << "Enter dt(fs): ";
  scanf("%lf  %*s", &dt);
  dt = dt * fs2au;

  cout << "Writing step (must be a factor of N): ";
  scanf("%zu  %*s", &step);


  cout << "Is imaginary(1) or real(2) propagation?: ";
  scanf("%d  %*s", &im_re);
  //If the propagation is imaginary the field is 0
  if (im_re == 1)
  {
    E0 = 0;
  }

  cout << "Number of OCT iterations: ";
  scanf("%d  %*s", &iter);

  cout << "Do we print (1) the time evolution of the wave function?: ";
  scanf("%d  %*s", &print);
}

void iter_monitor(int iter, double *t, double **monit, double a0, const char *file_name, size_t N, size_t n_monit, double *y, double *yf, dif_eq func, size_t n_eq)
{

  double *t_asc; //Ascending time
  double *e_asc; //Corresponding ascending time field
  double *e_2; // The square of the electric field
  double *e_t_out; //The field taken from monitor matrix

  double integral; //Here I storage the integral of the field from 0 to T
  double over; // Here I storage the overlap with the final state

  //Allocating memory
  vector_double_allocation(t_asc, N + 1);
  vector_double_allocation(e_asc, N + 1);
  vector_double_allocation(e_2,N+1);
  vector_allocation(e_t_out, N + 1);

  //Extracting the field from monit
  take_col(e_t_out, monit, N + 1, n_monit, 0);

  //Ordering the time in ascending order.
  order_field(t_asc, e_asc, t, e_t_out, N + 1);

  for(int i =0; i< N+1;i++)
  {
    e_2[i]=pow(e_asc[i],2.);
  }


  integral=simpson(e_2,t_asc[0],t_asc[N],N);
  over=func.overlap(y,yf,n_eq);


  FILE *data;

  if (iter==0)
  {
    data = fopen(file_name, "w");
    fprintf(data, "%s", "# iter   int_e  over   over+a0*int_e");
    fprintf(data,"\n %i   %e   %e   %e", iter, integral, over, over + a0 * integral);
  }
  else
  {
    data = fopen(file_name, "a");
    fprintf(data,"\n %i  %e    %e   %e", iter, integral, over, over + a0 * integral);
  }

  fclose(data);

  delete[] e_t_out;
  delete[] t_asc;
  delete[] e_asc;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main()
{
    size_t n_eq=4;    //Number of equations to be solved

    // Particle data
    double q,m;
    // Potential data
    double vmax,a;
    //Field parameters
    double E0,w,phi,a0;
    // Initial state data
    double alfa_0,beta_0,x0,px0;
    // Final state data
    double alfa_f, beta_f, xf, pxf;
    //Simulation data
    double ti,dt;
    size_t N,step;
    int im_re,iter,print;

    read_initial_data(q,m,vmax,a,E0,w,phi,a0,alfa_0,beta_0,x0,px0,alfa_f,beta_f,xf,pxf,ti,N,dt,step,im_re,iter,print);



    //Allocating initial and final vectors where I store initial and final set of parameters
    double *y0;
    double *yf;
    double *yzero;
    double *yaux;
    double *lag_init;

    double *yin;
    double *yout;

    vector_allocation(y0, n_eq);
    vector_allocation(yf, n_eq);
    vector_allocation(yzero, n_eq);
    vector_allocation(yaux, n_eq);
    vector_allocation(lag_init, n_eq);

    vector_allocation(yin, 2 * n_eq);
    vector_allocation(yout, 2 * n_eq);

    //This is a dummy zero state to use in the first propagation
    vector_cero_initialization(yzero, n_eq);

    double for_back;  //A flag that identifies forward (1) or backwards (-1) popagation
    double tf = N * dt; //The final instant of time

    double *t_in;   // Here I storage the time
    double *e_t_in; // Here I storage the time dependence of the input electrical field

    double *t_out;   // Here I storage the time
    double *e_t_out; // Here I storage the time dependence of the output electrical field

    double **monit;  // This is a matrix that stores the evaluation of some multidimentional function of the parameters every time step
    size_t n_monit = dif_eq::n_monit; // Dimension of the multidimensional function

    gsl_interp_accel *acc;      // A pointer to an interpolation accelerator
    gsl_spline *efield_spline;  // A pointer to the interpolation

    vector_double_allocation(t_in, N + 1);
    vector_double_allocation(e_t_in, N + 1);

    vector_double_allocation(t_out, N + 1);
    vector_double_allocation(e_t_out, N + 1);

    //This where I store a user defined function evaluation as a function of time
    matrix_allocation(monit, N + 1, n_monit);

    //Setting the initial and final state
    set_state(y0, alfa_0, beta_0, x0, px0, n_eq);
    set_state(yf, alfa_f, beta_f, xf, pxf, n_eq);

    ////////////////////////////////////////////////////////////////////////////////////////////
    //////////  Performing first propagation  //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    // Defining where I will output the data
    ostringstream monitor_file;
    monitor_file << "results/t_monitor_" << 0 << ".txt";
    const char monitor_file_header[52]="#t(au)        E(au)          V(Eh)          Overlap";

    ostringstream out_file;
    out_file << "results/t_Yt_" << 0 << ".txt";

    ostringstream iter_monit_file;
    iter_monit_file << "results/iter_monit.txt";

    //Initial field
    for (size_t i = 0; i < N + 1; i++)
    {
      t_in[i] = ti + i * dt;
      e_t_in[i] = E0 * cos(w * (ti + i * dt) + phi);
    }

    //Constructing first differential equation
    for_back=1;
    dif_eq func = construct_dif_eq(acc, efield_spline, q, m, vmax, a, im_re, t_in, e_t_in, a0, N, tf,for_back);

    //Creating initial state for differential equation solver
    join_vectors(yin, y0, 0, 4, yzero, 0, 4);

    if (im_re == 1)
    {
      //Solving differential equations for imaginary time.
      Solve_DEq('a', func, t_out, monit, n_monit, yin, yout, ti, N, for_back*dt, step, out_file.str().c_str(), print, 2 * n_eq);
      iter = 0; //Iter is set to zero because I'm not doing any iteration for imaginary time propagation
    }
    if (im_re == 2)
    {
      //Solving differential equations for real time
      Solve_DEq('a', func, t_out, monit, n_monit, yin, yout, ti, N, for_back*dt, step, out_file.str().c_str(), print, 2 * n_eq);
    }
    take_col(e_t_out,monit,N+1,n_monit,0);

    //Writing monitor for the first iteration
    write_monit_to_file(t_out, N + 1, monit, n_monit, monitor_file.str().c_str(), monitor_file_header);

    take(yaux, yout, 0, 4);
    iter_monitor(0, t_out, monit, a0, iter_monit_file.str().c_str(), N, n_monit, yaux, yf, func, n_eq);

    double init_time; //This is defined to alternate between the initial time and the final time

    gsl_spline_free(efield_spline); //Free the spline pointer
    gsl_interp_accel_free(acc);     //Free the accelerator pointer

    

    ////////////////////////////////////////////////////////////////////////////////////////////
    //////////  Performing iteration propagation  //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    for (int i = 1; i < iter; i++)
    {
      // Defining where I will output the data
      // ostringstream spline_file;
      // spline_file << "results/spline_" << i << ".txt";


      // i odd => backward (-1)  ,   i even => forward (1)
      for_back = (2 * ((i+1) % 2) - 1);
      if(for_back==1)
      {
        take(yaux, yout, 4, 8);
        join_vectors(yin, y0, 0, 4, yaux, 0, 4);
        init_time=ti;
      }
      else if (for_back == -1)
      {
        take(yaux, yout, 0, 4);

        func.lagrange_projection(lag_init,yaux,yf,4);

        join_vectors(yin, yaux, 0, 4 , lag_init,0,4);

        init_time = tf;
      }

      if (i==1)//For the first propagation the input field is the initial field
      {
        func = construct_dif_eq(acc, efield_spline, q, m, vmax, a, im_re, t_in, e_t_in, a0, N, tf, for_back);
      }
      else// For the rest is the output of the previous propagation
      {
        func = construct_dif_eq(acc, efield_spline, q, m, vmax, a, im_re, t_out, e_t_out, a0, N, tf, for_back);
      }
      

  
      

      // Defining where I will output the data
      ostringstream monitor_file;
      monitor_file << "results/t_monitor_" << i << ".txt";

      ostringstream out_file;
      out_file << "results/t_Yt_" << i << ".txt";

      //Solving differential equations
      Solve_DEq('a', func, t_out, monit, n_monit, yin, yout, init_time, N, for_back * dt, step, out_file.str().c_str(), print, 2 * n_eq);
      take_col(e_t_out, monit, N + 1, n_monit, 0);


      //Writing monitor
      write_monit_to_file(t_out, N + 1, monit, n_monit, monitor_file.str().c_str(), monitor_file_header);

      if (for_back==1)
      {
        take(yaux,yout,0,4);
        iter_monitor(i, t_out, monit, a0, iter_monit_file.str().c_str(), N, n_monit,yaux,yf,func,n_eq);
      }

      cout<<endl<<endl<<"!!!!!!  Iteration: "<<i<< endl;
      

      gsl_spline_free(efield_spline); //Free the spline pointer
      gsl_interp_accel_free(acc);     //Free the accelerator poiunter
      }

}
