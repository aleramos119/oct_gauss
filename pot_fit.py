import numpy as np

from scipy import interpolate
from scipy import optimize
from scipy import integrate

from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])

def Vas(x,vmax,a,b):
    return (vmax/a**4)*(x**2-a**2)**2+b*x/a


def gauss(x,g,b,x0):
    return g*np.e**(-b*(x-x0)**2)


def sum_gauss_5(x,g1,b1,x01,g2,b2,x02,g3,b3,x03,g4,b4,x04,g5):
    return gauss(x,g1,b1,x01)+gauss(x,g2,b2,x02)+gauss(x,g3,b3,x03)+gauss(x,g4,b4,x04)+gauss(x,g5,0,0)

def sum_gauss_4(x,g1,b1,x01,g2,b2,x02,g3,b3,x03,g4,b4,x04):
    return gauss(x,g1,b1,x01)+gauss(x,g2,b2,x02)+gauss(x,g3,b3,x03)+gauss(x,g4,b4,x04)

def sum_gauss_3(x,g1,b1,x01,g2,b2,x02,g3,b3,x03):
    return gauss(x,g1,b1,x01)+gauss(x,g2,b2,x02)+gauss(x,g3,b3,x03)



### Fit with 5 gaussians
xmax=2
x=np.linspace(-xmax,xmax,1000)

dg=1
db=1
dx=1

good4=np.array([31.00001469,  1.39712198, -2.98085171, -1.52881858,  1.65840237,-1.14213435, -1.52881913,  1.65840138,  1.14213468, 31.00008137,1.39711973,  2.98085324,  1.34845505])

init=[i for i in good4[0:13]]
bounds=[dg,db,dx,dg,db,dx,dg,db,dx,dg,db,dx,dg]
lbounds=[good1[i]-bounds[i]  for i in range(0,13)]
ubounds=[good1[i]+bounds[i]  for i in range(0,13)]


best_vals_5, covar_5 = curve_fit(sum_gauss_5, x, Vas(x,1,1,0), p0=init,maxfev=100000,bounds=(lbounds,ubounds))

xmax=3
x=np.linspace(-xmax,xmax,1000)
vmax=1.
a=1.
plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss_5(x,best_vals_5[0]*vmax,best_vals_5[1]/a**2,best_vals_5[2]*a, best_vals_5[3]*vmax,best_vals_5[4]/a**2,best_vals_5[5]*a, best_vals_5[6]*vmax,best_vals_5[7]/a**2,best_vals_5[8]*a, best_vals_5[9]*vmax,best_vals_5[10]/a**2,best_vals_5[11]*a,best_vals_5[12]*vmax))

for i in range(0,4):
    plt.plot(x,gauss(x,best_vals_5[3*i+0],best_vals_5[3*i+1],best_vals_5[3*i+2]),label=str(best_vals_5[3*i+0])+"   "+str(best_vals_5[3*i+1])+"   "+str(best_vals_5[3*i+2]))
    plt.legend()


## Integrating the error to see which approximation is better
def integ(f,xi,xf):
    i=integrate.quad(f,xi,xf)
    return i



err2=integ(lambda x:np.abs(Vas(x,1,1,0)-sum_gauss_5(x,good2[0],good2[1],good2[2], good2[3],good2[4],good2[5], good2[6],good2[7],good2[8], good2[9],good2[10],good2[11],good2[12])),-2,2)[0]
err3=integ(lambda x:np.abs(Vas(x,1,1,0)-sum_gauss_5(x,good3[0],good3[1],good3[2], good3[3],good3[4],good3[5], good3[6],good3[7],good3[8], good3[9],good3[10],good3[11],good3[12])),-2,2)[0]
err4=integ(lambda x:np.abs(Vas(x,1,1,0)-sum_gauss_5(x,good4[0],good4[1],good4[2], good4[3],good4[4],good4[5], good4[6],good4[7],good4[8], good4[9],good4[10],good4[11],good4[12])),-2,2)[0]


err2

err3

err4






### Fit with 4 gaussians
xmax=1.6
x=np.linspace(-xmax,xmax,1000)

best_vals_4, covar_4 = curve_fit(sum_gauss_4, x, Vas(x,1,1,0), p0=[10,10,-2, -1,1,-1, -1,1,1, 10,10,2],maxfev=100000,bounds=([0,0,-10, -np.inf,0,-10, -np.inf,0,-10, 0,0,0], [np.inf,np.inf,10, np.inf,np.inf,10, np.inf,np.inf,10, np.inf,np.inf,np.inf]))

xmax=3
x=np.linspace(-xmax,xmax,1000)
plt.plot(x,Vas(x,1,1,0),x,sum_gauss_4(x,best_vals_4[0],best_vals_4[1],best_vals_4[2], best_vals_4[3],best_vals_4[4],best_vals_4[5], best_vals_4[6],best_vals_4[7],best_vals_4[8], best_vals_4[9],best_vals_4[10],best_vals_4[11]))

for i in range(0,4):
    plt.plot(x,gauss(x,best_vals_4[3*i+0],best_vals_4[3*i+1],best_vals_4[3*i+2]),label=str(best_vals_4[3*i+0])+"   "+str(best_vals_4[3*i+1])+"   "+str(best_vals_4[3*i+2]))
    plt.legend()





### Fit with 3 gaussians
xmax=1.5
x=np.linspace(-xmax,xmax,1000)
best_vals_3, covar_3 = curve_fit(sum_gauss_3, x, Vas(x,1,1,0), p0=[1000,1,-2, 1,1,0, 1000,1,2],maxfev=100000,bounds=([0,0,-10,0,0,-1,0,0,0], [np.inf,np.inf,0,np.inf,np.inf,1,np.inf,np.inf,10]))
xmax=3
x=np.linspace(-xmax,xmax,1000)
plt.plot(x,Vas(x,1,1,0),x,sum_gauss_3(x,best_vals_3[0],best_vals_3[1],best_vals_3[2], best_vals_3[3],best_vals_3[4],best_vals_3[5], best_vals_3[6],best_vals_3[7],best_vals_3[8]))
for i in range(0,3):
    plt.plot(x,gauss(x,best_vals_3[3*i+0],best_vals_3[3*i+1],best_vals_3[3*i+2]),label=str(best_vals_3[3*i+0])+"   "+str(best_vals_3[3*i+1])+"   "+str(best_vals_3[3*i+2]))
    plt.legend()
