(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     24314,        801]
NotebookOptionsPosition[     23089,        754]
NotebookOutlinePosition[     23428,        769]
CellTagsIndexPosition[     23385,        766]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Bp", "[", 
     RowBox[{"a_", ",", "bp_", ",", "x0_", ",", "xp_"}], "]"}], ":=", 
    RowBox[{
     FractionBox[
      RowBox[{"2", " ", "a", " ", "bp"}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], " ", "+", "bp"}]], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Dp", "[", 
     RowBox[{"a_", ",", "gp_", ",", "bp_", ",", "x0_", ",", "xp_"}], "]"}], ":=", 
    RowBox[{"gp", " ", 
     SuperscriptBox["E", 
      RowBox[{"-", 
       RowBox[{"Bp", "[", 
        RowBox[{"a", ",", "bp", ",", "x0", ",", "xp"}], "]"}]}]], 
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox[
        RowBox[{"2", " ", "a", " ", "bp"}], 
        RowBox[{
         RowBox[{"2", " ", "a"}], " ", "+", "bp"}]], ")"}], 
      RowBox[{"3", "/", "2"}]]}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dVda", "[", 
     RowBox[{
     "a_", ",", "b_", ",", "x0_", ",", "p0_", ",", "gp_", ",", "bp_", ",", 
      "xp_"}], "]"}], ":=", 
    RowBox[{
     RowBox[{"Dp", "[", 
      RowBox[{"a", ",", "gp", ",", "bp", ",", "x0", ",", "xp"}], "]"}], 
     RowBox[{"(", 
      RowBox[{
       FractionBox["bp", 
        RowBox[{"4", " ", 
         SuperscriptBox["a", "2"]}]], "-", 
       RowBox[{
        FractionBox[
         SuperscriptBox["bp", "2"], 
         RowBox[{"a", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"2", "a"}], "+", "bp"}], ")"}]}]], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dVdx0", "[", 
     RowBox[{
     "a_", ",", "b_", ",", "x0_", ",", "p0_", ",", "gp_", ",", "bp_", ",", 
      "xp_"}], "]"}], ":=", 
    RowBox[{
     RowBox[{"-", "2"}], "bp", " ", 
     RowBox[{"Dp", "[", 
      RowBox[{"a", ",", "gp", ",", "bp", ",", "x0", ",", "xp"}], "]"}], 
     RowBox[{"(", 
      RowBox[{"x0", "-", "xp"}], ")"}]}]}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"re2", "[", 
     RowBox[{
     "a_", ",", "b_", ",", "x0_", ",", "p0_", ",", "gp_", ",", "bp_", ",", 
      "xp_"}], "]"}], ":=", 
    RowBox[{
     FractionBox[
      RowBox[{"-", "2"}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"b", "-", "bf"}], ")"}], "2"]}]], 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"af", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["a", "2"], "+", 
          SuperscriptBox["b", "2"]}], ")"}]}], "+", 
       RowBox[{"a", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["af", "2"], "+", 
          SuperscriptBox["bf", "2"]}], ")"}], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"x0", "-", "x0f"}], ")"}], "2"]}], "+", 
       RowBox[{
        FractionBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"a", " ", "bf"}], "+", 
           RowBox[{"af", " ", "b"}]}], ")"}], "h"], 
        RowBox[{"(", 
         RowBox[{"p0", "-", "p0f"}], ")"}], 
        RowBox[{"(", 
         RowBox[{"x0", "-", "x0f"}], ")"}]}], "+", 
       RowBox[{
        FractionBox[
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], "2"], 
         RowBox[{"4", 
          SuperscriptBox["h", "2"]}]], 
        RowBox[{"(", 
         RowBox[{"a", "+", "af"}], ")"}]}]}], ")"}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"fact", "[", 
     RowBox[{
     "a_", ",", "b_", ",", "x0_", ",", "p0_", ",", "gp_", ",", "bp_", ",", 
      "xp_"}], "]"}], ":=", 
    SqrtBox[
     FractionBox[
      RowBox[{"4", " ", "a", " ", "af"}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"b", "-", "bf"}], ")"}], "2"]}]]]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"psif", "[", 
     RowBox[{
     "a_", ",", "b_", ",", "x0_", ",", "p0_", ",", "gp_", ",", "bp_", ",", 
      "xp_"}], "]"}], ":=", 
    RowBox[{
     SqrtBox[
      FractionBox[
       RowBox[{"4", " ", "a", " ", "af"}], 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"b", "-", "bf"}], ")"}], "2"]}]]], 
     SuperscriptBox["E", 
      RowBox[{
       FractionBox[
        RowBox[{"-", "2"}], 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"b", "-", "bf"}], ")"}], "2"]}]], 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"af", " ", 
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["a", "2"], "+", 
            SuperscriptBox["b", "2"]}], ")"}]}], "+", 
         RowBox[{"a", " ", 
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["af", "2"], "+", 
            SuperscriptBox["bf", "2"]}], ")"}], 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x0", "-", "x0f"}], ")"}], "2"]}], "+", 
         RowBox[{
          FractionBox[
           RowBox[{"(", 
            RowBox[{
             RowBox[{"a", " ", "bf"}], "+", 
             RowBox[{"af", " ", "b"}]}], ")"}], "h"], 
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], 
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}]}], "+", 
         RowBox[{
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"p0", "-", "p0f"}], ")"}], "2"], 
           RowBox[{"4", 
            SuperscriptBox["h", "2"]}]], 
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}]}]}], ")"}]}]]}]}]}]}]], "Input",
 CellChangeTimes->{{3.770806368003377*^9, 3.770806503036191*^9}, {
  3.77080690963899*^9, 3.770806927743023*^9}, {3.770807445665688*^9, 
  3.770807446678505*^9}, {3.770808896195902*^9, 3.77080892631149*^9}, {
  3.770810449708417*^9, 3.770810454302006*^9}, {3.770811842319765*^9, 
  3.7708119373770227`*^9}, {3.770816872444278*^9, 3.770817192451091*^9}, {
  3.770817369453927*^9, 3.770817375757162*^9}, {3.770817484817493*^9, 
  3.770817524056712*^9}, {3.770817710220504*^9, 3.770817733724039*^9}, {
  3.7708195667077713`*^9, 3.770819567228135*^9}}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.770817489736685*^9}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{{3.770808966893795*^9, 3.77080902505326*^9}, {
   3.770809150109152*^9, 3.770809176845354*^9}, {3.770810198000349*^9, 
   3.7708102100095654`*^9}, {3.770810264535899*^9, 3.770810267999531*^9}, {
   3.7708103204765663`*^9, 3.7708103357963047`*^9}, 3.770811049611143*^9, {
   3.770811132425727*^9, 3.770811142129382*^9}, 3.7708118407505617`*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     SubscriptBox["\[PartialD]", "a"], 
     RowBox[{"dVda", "[", 
      RowBox[{
      "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
      "]"}]}], "*", 
    FractionBox[
     RowBox[{"4", " ", 
      SuperscriptBox["a", "2"]}], "h"]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{
  3.7708065068765593`*^9, {3.7708065712620583`*^9, 3.770806572841704*^9}, {
   3.770806785031782*^9, 3.770806863670669*^9}, {3.7708074293977213`*^9, 
   3.770807460548479*^9}, 3.770808960610669*^9, {3.770810697259305*^9, 
   3.770810764196321*^9}, {3.7708110671186523`*^9, 3.770811099783165*^9}, {
   3.770811130138187*^9, 3.770811146688905*^9}, {3.770811484183959*^9, 
   3.770811484656911*^9}, {3.7708115152327023`*^9, 3.770811516174767*^9}, {
   3.77081173589237*^9, 3.770811746899506*^9}, {3.770815344244711*^9, 
   3.770815345519156*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   SuperscriptBox["bp", "2"], " ", 
   SqrtBox[
    FractionBox[
     RowBox[{"a", " ", "bp"}], 
     RowBox[{
      RowBox[{"2", " ", "a"}], "+", "bp"}]]], " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "a", " ", "bp", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"x0", "-", "xp"}], ")"}], "2"]}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]]}]], " ", "gp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["bp", "3"]}], "+", 
     RowBox[{"32", " ", 
      SuperscriptBox["a", "3"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"2", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}], "-", 
     RowBox[{"4", " ", "a", " ", 
      SuperscriptBox["bp", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"3", "+", 
        RowBox[{"2", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}], "+", 
     RowBox[{"4", " ", 
      SuperscriptBox["a", "2"], " ", "bp", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "9"}], "+", 
        RowBox[{"4", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}], "+", 
        RowBox[{"4", " ", 
         SuperscriptBox["bp", "2"], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "4"]}]}], ")"}]}]}], ")"}]}], 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", "a"}], "+", "bp"}], ")"}], "4"], " ", 
   "h"}]]], "Output",
 CellChangeTimes->{3.770811752940915*^9, 3.770815346827198*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     SubscriptBox["\[PartialD]", "x0"], 
     RowBox[{"dVda", "[", 
      RowBox[{
      "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
      "]"}]}], "*", 
    FractionBox[
     RowBox[{"4", " ", 
      SuperscriptBox["a", "2"]}], "h"]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.770815350802599*^9, 3.7708153601830883`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"8", " ", 
   SqrtBox["2"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"a", " ", "bp"}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]], ")"}], 
    RowBox[{"7", "/", "2"}]], " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "a", " ", "bp", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"x0", "-", "xp"}], ")"}], "2"]}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]]}]], " ", "gp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "3"}], " ", "bp"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "6"}], "+", 
        RowBox[{"4", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}]}], ")"}], " ", 
   
   RowBox[{"(", 
    RowBox[{"x0", "-", "xp"}], ")"}]}], 
  RowBox[{"a", " ", "h"}]]], "Output",
 CellChangeTimes->{3.770815361041852*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "a"], 
    RowBox[{"dVdx0", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.770812053138267*^9, 3.770812076250646*^9}, {
  3.7708121349178677`*^9, 3.770812135626814*^9}, {3.7708154165694036`*^9, 
  3.770815416858396*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", 
   SqrtBox["2"], " ", 
   SuperscriptBox["bp", "3"], " ", 
   SqrtBox[
    FractionBox[
     RowBox[{"a", " ", "bp"}], 
     RowBox[{
      RowBox[{"2", " ", "a"}], "+", "bp"}]]], " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "a", " ", "bp", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"x0", "-", "xp"}], ")"}], "2"]}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]]}]], " ", "gp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "3"}], " ", "bp"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "6"}], "+", 
        RowBox[{"4", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}]}], ")"}], " ", 
   
   RowBox[{"(", 
    RowBox[{"x0", "-", "xp"}], ")"}]}], 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "a"}], "+", "bp"}], ")"}], "3"]]], "Output",
 CellChangeTimes->{{3.77081206726265*^9, 3.770812076670909*^9}, 
   3.7708121368851137`*^9, 3.7708154252627783`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "x0"], 
    RowBox[{"dVdx0", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.770815438778767*^9, 3.7708154417632113`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"4", " ", 
   SqrtBox["2"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"a", " ", "bp"}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]], ")"}], 
    RowBox[{"5", "/", "2"}]], " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "a", " ", "bp", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"x0", "-", "xp"}], ")"}], "2"]}], 
      RowBox[{
       RowBox[{"2", " ", "a"}], "+", "bp"}]]}]], " ", "gp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "bp"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "2"}], "+", 
        RowBox[{"4", " ", "bp", " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "xp"}], ")"}], "2"]}]}], ")"}]}]}], ")"}]}], 
  "a"]], "Output",
 CellChangeTimes->{3.770815442397974*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "a"], 
    RowBox[{"re2", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.770819545750593*^9, 3.770819551136962*^9}, {
  3.770819581266839*^9, 3.770819589158647*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"b", "-", "bf"}], ")"}], "2"]}], ")"}], "2"]], 
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"b", "-", "bf"}], ")"}], "2"]}], ")"}]}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", "a", " ", "af"}], "+", 
        FractionBox[
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], "2"], 
         RowBox[{"4", " ", 
          SuperscriptBox["h", "2"]}]], "+", 
        FractionBox[
         RowBox[{"bf", " ", 
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}]}], "h"], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["af", "2"], "+", 
           SuperscriptBox["bf", "2"]}], ")"}], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}], "2"]}]}], ")"}]}], "+", 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{"a", "+", "af"}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"af", " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["a", "2"], "+", 
           SuperscriptBox["b", "2"]}], ")"}]}], "+", 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}], " ", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"p0", "-", "p0f"}], ")"}], "2"]}], 
         RowBox[{"4", " ", 
          SuperscriptBox["h", "2"]}]], "+", 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"af", " ", "b"}], "+", 
            RowBox[{"a", " ", "bf"}]}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}]}], "h"], "+", 
        RowBox[{"a", " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["af", "2"], "+", 
           SuperscriptBox["bf", "2"]}], ")"}], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}], "2"]}]}], ")"}]}]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{{3.770819583996059*^9, 3.770819590605762*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "b"], 
    RowBox[{"re2", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.7708195960251513`*^9, 3.7708196032771797`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"b", "-", "bf"}], ")"}], "2"]}], ")"}]}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", "af", " ", "b"}], "+", 
        FractionBox[
         RowBox[{"af", " ", 
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}]}], "h"]}], ")"}]}], "+", 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{"b", "-", "bf"}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"af", " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["a", "2"], "+", 
           SuperscriptBox["b", "2"]}], ")"}]}], "+", 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{"a", "+", "af"}], ")"}], " ", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"p0", "-", "p0f"}], ")"}], "2"]}], 
         RowBox[{"4", " ", 
          SuperscriptBox["h", "2"]}]], "+", 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"af", " ", "b"}], "+", 
            RowBox[{"a", " ", "bf"}]}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"p0", "-", "p0f"}], ")"}], " ", 
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}]}], "h"], "+", 
        RowBox[{"a", " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["af", "2"], "+", 
           SuperscriptBox["bf", "2"]}], ")"}], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x0", "-", "x0f"}], ")"}], "2"]}]}], ")"}]}]}], ")"}]}], 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"b", "-", "bf"}], ")"}], "2"]}], ")"}], "2"]]], "Output",
 CellChangeTimes->{3.770819603954904*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "x0"], 
    RowBox[{"re2", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.77081960646964*^9, 3.770819611253537*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{"(", 
     RowBox[{
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"af", " ", "b"}], "+", 
          RowBox[{"a", " ", "bf"}]}], ")"}], " ", 
        RowBox[{"(", 
         RowBox[{"p0", "-", "p0f"}], ")"}]}], "h"], "+", 
      RowBox[{"2", " ", "a", " ", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["af", "2"], "+", 
         SuperscriptBox["bf", "2"]}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{"x0", "-", "x0f"}], ")"}]}]}], ")"}]}], 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"b", "-", "bf"}], ")"}], "2"]}]]}]], "Output",
 CellChangeTimes->{3.7708196118478603`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "p0"], 
    RowBox[{"re2", "[", 
     RowBox[{
     "a", ",", "b", ",", "x0", ",", "p0", ",", "gp", ",", "bp", ",", "xp"}], 
     "]"}]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.770819614395403*^9, 3.770819617258273*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"a", "+", "af"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{"p0", "-", "p0f"}], ")"}]}], "+", 
    RowBox[{"2", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"af", " ", "b"}], "+", 
       RowBox[{"a", " ", "bf"}]}], ")"}], " ", "h", " ", 
     RowBox[{"(", 
      RowBox[{"x0", "-", "x0f"}], ")"}]}]}], 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"a", "+", "af"}], ")"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"b", "-", "bf"}], ")"}], "2"]}], ")"}], " ", 
    SuperscriptBox["h", "2"]}]]}]], "Output",
 CellChangeTimes->{3.770819617790513*^9}]
}, Open  ]]
},
WindowSize->{1280, 691},
WindowMargins->{{Automatic, 0}, {Automatic, -1}},
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 6596, 198, 523, "Input"],
Cell[7157, 220, 87, 1, 55, "Input"],
Cell[7247, 223, 408, 5, 55, "Input"],
Cell[CellGroupData[{
Cell[7680, 232, 943, 21, 79, "Input"],
Cell[8626, 255, 1872, 60, 83, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10535, 320, 448, 13, 79, "Input"],
Cell[10986, 335, 1054, 36, 73, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12077, 376, 441, 11, 56, "Input"],
Cell[12521, 389, 1176, 39, 83, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13734, 433, 342, 9, 56, "Input"],
Cell[14079, 444, 942, 32, 73, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15058, 481, 386, 10, 56, "Input"],
Cell[15447, 493, 2710, 86, 109, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18194, 584, 341, 9, 56, "Input"],
Cell[18538, 595, 2193, 70, 72, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20768, 670, 337, 9, 56, "Input"],
Cell[21108, 681, 835, 28, 62, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21980, 714, 338, 9, 58, "Input"],
Cell[22321, 725, 752, 26, 56, "Output"]
}, Open  ]]
}
]
*)

