
class dif_eq
{
  private : 
      double q, m, vmax, a;
      double a0;
      double T;
      int for_back; //This is an indicator if I'm propagating forward (1) or backwards (-1). This is important for E12. In the forward propagation 
      int im_re;   //If I do imaginary time (1) or real time(2) propagation

      //This are the gaussian expantion of the potential
      size_t n_gauss = 5;                                                                                                                  /*!< Number of gaussians used for expanding the external potential */
      double g[5] = {31.00001469 * vmax, -1.52881858 * vmax, -1.52881858 * vmax, 31.00001469 * vmax, 1.34845505 * vmax};                   /*!< Gaussian potential expansion height coefficients */
      double b[5] = {1.39712198 * pow(a, -2), 1.65840237 * pow(a, -2), 1.65840237 * pow(a, -2), 1.39712198 * pow(a, -2), 0. * pow(a, -2)}; /*!<  Gaussian potential expansion wide coefficients */
      double xp[5] = {-2.98085171 * a, -1.14213435 * a, 1.14213435 * a, 2.98085171 * a, 0. * a};                                           /*!<  Gaussian potential expansion center coefficients */

      //This function is the previous electric field. I define it in the initialization list of the class constructor
      std::function<double(double)> E; //Declaring the function E(t)


      /**
       * @brief This is a function that defines the shape of the pulse
       * 
       * @param t The current instant of time
       * @param T The final time of the propagation 
       * @param a0 The penalty factor to avoid unfisical large electric fields
       * @return double Returns the evaluation of the function
       */
      double s(double t, double T, double a0)
      {
        //return pow( sin(PI*t/T) ,2.)/(2*a0);
        return 1/(2*a0);
      }


      ///Expresions depending on only one state
      double Bp(double a, double x0, double xp, double bp)
      {
        return (2 * a * bp) * pow(x0 - xp, 2) / (2 * a + bp);
      }

      double Dp(double a, double x0, double xp, double bp, double gp)
      {
        return gp * exp(-Bp(a, x0, xp, bp)) * pow(2 * a / (2 * a + bp), 3. / 2.);
      }

      double dV_da(double a, double x0, double gp, double bp, double xp)
      {
        return Dp(a, x0, xp, bp, gp) * (bp / (4 * pow(a, 2)) - pow(bp * (x0 - xp), 2) / (a * (2 * a + bp)));
      }

      double dV_dx(double a, double x0, double gp, double bp, double xp)
      {
        return -2 * Dp(a, x0, xp, bp, gp) * bp * (x0 - xp);
      }

      double V(double a, double x0, double gp, double bp, double xp)
      {
        return gp * exp(-Bp(a, x0, xp, bp)) * pow(2 * a / (2 * a + bp), 1. / 2.);
      }

      double dfb_da(double a1, double x01, double gp, double bp, double xp)
      {
        return -((sqrt(2)*pow(bp,2)*sqrt(a1/(2*a1 + bp))*gp*(3*pow(bp,2) - 12*a1*bp*(-1 + 2*bp*pow(x01 - xp,2)) + 4*pow(a1,2)*(3 - 12*bp*pow(x01 - xp,2) + 4*pow(bp,2)*pow(x01 - xp,4))))/(pow(2*a1 + bp,4)*exp((2*a1*bp*pow(x01 - xp,2))/(2*a1 + bp))));
      }

      double dfp0_da(double a1, double x01, double gp, double bp, double xp)
      {
        return (-2*sqrt(2)*pow(bp,2)*sqrt(a1/(2*a1 + bp))*gp*(-3*bp + a1*(-6 + 4*bp*pow(x01 - xp,2)))*(x01 - xp))/(pow(2*a1 + bp,3)*exp((2*a1*bp*pow(x01 - xp,2))/(2*a1 + bp)));
      }

      double dfb_dx0(double a1, double x01, double gp, double bp, double xp)
      {
        return (-8*sqrt(2)*pow(bp,2)*pow(a1/(2*a1 + bp),3.5)*gp*(-3*bp + a1*(-6 + 4*bp*pow(x01 - xp,2)))*(x01 - xp))/(a1*exp((2*a1*bp*pow(x01 - xp,2))/(2*a1 + bp)));
      }

      double dfp0_dx0(double a1, double x01, double gp, double bp, double xp)
      {
        return (-4*sqrt(2)*bp*pow(a1/(2*a1 + bp),2.5)*gp*(-bp + a1*(-2 + 4*bp*pow(x01 - xp,2))))/(a1*exp((2*a1*bp*pow(x01 - xp,2))/(2*a1 + bp)));
      }



      //// Expresions depending on two states
      double exp_2re(double a1, double b1, double x01, double p01, double a2, double b2, double x02, double p02)
      {
        double gam1_gam2; //Iqual to |Gam1* + Gam2|
        double gam1_2;
        double gam2_2;
        double re2;

        gam1_gam2 = mod(a1 + a2, b1 - b2);
        gam1_2 = mod_2(a1, b1);
        gam2_2 = mod_2(a2, b2);

        re2 = -2 * ((a1 * gam2_2 + a2 * gam1_2) * pow(x01 - x02, 2.) + (a1 * b2 + a2 * b1) * (p01 - p02) * (x01 - x02) + (a1 + a2) * pow(p01 - p02, 2.) / 4) / pow(gam1_gam2, 2);

        return exp(re2);
      }
      double fact(double a1, double b1, double a2, double b2)
      {
        return sqrt(4 * a1 * a2) / mod(a1 + a2, b1 - b2);
      }


      double dexp_2re_da1(double a1, double b1, double x01, double p01, double a2, double b2, double x02, double p02)
      {
        return exp_2re(a1, b1, x01, p01, a2, b2, x02, p02) * ((-2*(pow(p01 - p02,2)/4. + b2*(p01 - p02)*(x01 - x02) + (2*a1*a2 + pow(a2,2) + pow(b2,2))*pow(x01 - x02,2)))/(pow(a1 + a2,2) + pow(b1 - b2,2)) + (4*(a1 + a2)*(((a1 + a2)*pow(p01 - p02,2))/4. + (a2*b1 + a1*b2)*(p01 - p02)*(x01 - x02) + (a2*(pow(a1,2) + pow(b1,2)) + a1*(pow(a2,2) + pow(b2,2)))*pow(x01 - x02,2)))/pow(pow(a1 + a2,2) + pow(b1 - b2,2),2));
      }
      double dexp_2re_db1(double a1, double b1, double x01, double p01, double a2, double b2, double x02, double p02)
      {
        return exp_2re(a1, b1, x01, p01, a2, b2, x02, p02) * ((-2*(a2*(p01 - p02)*(x01 - x02) + 2*a2*b1*pow(x01 - x02,2)))/(pow(a1 + a2,2) + pow(b1 - b2,2)) + (4*(b1 - b2)*(((a1 + a2)*pow(p01 - p02,2))/4. + (a2*b1 + a1*b2)*(p01 - p02)*(x01 - x02) + (a2*(pow(a1,2) + pow(b1,2)) + a1*(pow(a2,2) + pow(b2,2)))*pow(x01 - x02,2)))/pow(pow(a1 + a2,2) + pow(b1 - b2,2),2));
      }
      double dexp_2re_dx01(double a1, double b1, double x01, double p01, double a2, double b2, double x02, double p02)
      {
        return exp_2re(a1, b1, x01, p01, a2, b2, x02, p02) * ((-2*((a2*b1 + a1*b2)*(p01 - p02) + 2*(a2*(pow(a1,2) + pow(b1,2)) + a1*(pow(a2,2) + pow(b2,2)))*(x01 - x02)))/(pow(a1 + a2,2) + pow(b1 - b2,2)));
      }
      double dexp_2re_dp01(double a1, double b1, double x01, double p01, double a2, double b2, double x02, double p02)
      {
        return exp_2re(a1, b1, x01, p01, a2, b2, x02, p02) * ((-2*(((a1 + a2)*(p01 - p02))/2. + (a2*b1 + a1*b2)*(x01 - x02)))/(pow(a1 + a2,2) + pow(b1 - b2,2)));
      }


      double dfact_da1(double a1, double b1, double a2, double b2)
      {
        return (a2 * (pow(a2, 2) - pow(a1, 2) + pow(b1 - b2, 2)) ) / (pow(mod(a1+a2,b1-b2), 3) * sqrt(a1 * a2));
      }
      double dfact_db1(double a1, double b1, double a2, double b2)
      {
        return ((-2 * pow((a1 * a2) / mod_2(a1+a2,b1-b2), 1.5) * (b1 - b2)) / (a1 * a2));
      }
















    public:
      static const int n_monit = 6;

      dif_eq(double q, double m, double vmax, double a, int im_re, std::function<double(double)> E, double a0, double T, int for_back) : q(q), m(m), vmax(vmax), a(a), im_re(im_re), a0(a0), T(T), E(E), for_back(for_back)
      {}

      /**
         * @brief This function calculates the overlap between two states y1 and y2 
         * 
         * @param y1 Contains the parameters of the wave function 1
         * @param y2 Contains the parameters of the wave function 1
         * @param n The size of the parameters
         * @return double Returns the overlap
         */
      double overlap(double *y1, double *y2, size_t n)
      {
        //return fact(y1[0], y1[1], y2[0], y2[1]) * exp_2re(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3]);
        double sum=0;
        double w[4]={1,1,1,1};
        for (int i=0; i<n; i++)
        {
            sum = sum + w[i]*pow((y1[i]-y2[i]),2)/2;
        }
        return sum;
      }

      void lagrange_projection(double *lag, double *y1, double *y2, size_t n)
      {

        // lag[0] = dfact_da1(y1[0], y1[1], y2[0], y2[1]) * exp_2re(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3])   +   fact(y1[0], y1[1], y2[0], y2[1]) * dexp_2re_da1(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3]);

        // lag[1] = dfact_db1(y1[0], y1[1], y2[0], y2[1]) * exp_2re(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3])   +   fact(y1[0], y1[1], y2[0], y2[1]) * dexp_2re_db1(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3]);

        // lag[2] = fact(y1[0], y1[1], y2[0], y2[1]) * dexp_2re_dx01(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3]);

        // lag[3] = fact(y1[0], y1[1], y2[0], y2[1]) * dexp_2re_dp01(y1[0], y1[1], y1[2], y1[3], y2[0], y2[1], y2[2], y2[3]);

        lag[0] = -(y1[0] - y2[0]);
        lag[1] = -(y1[1] - y2[1]);
        lag[2] = -(y1[2] - y2[2]);
        lag[3] = -(y1[3] - y2[3]);
      }

        /**
       * @brief This an overload of the () operator so I can pass this function to Solve_DEq
       * 
       * @param f This is a pointer where I store the evaluation of the function
       * @param t The current instant of time 
       * @param Y The current values of the wavefunction parameters
       * @param n_eq The number of equations/parameters that I have
       */
        void operator()(double *der, double *monit, size_t n_monit, double t, double *Y, size_t n_eq)
        {

          if ((im_re != 1 && im_re != 2))
          {
            throw invalid_argument("!!!!!!!!!! im_re have to be 1 or 2 in class dif_eq !!!!!!!!!!!!!!!!");
          }

          double a1, b1, x01, p01, xa, xb, xx0, xp0; //The parameters that characterizes current wavefunction (1) propagation and previous wavefunction (2)                       //Iqual to |Gam1* + Gam2|
          double gam1_2;

          //This are the variables that I will monitor at each step
          double Ex; //The field to propagate Chi, the Lagrange multiplier
          double pot;

          double A_p1, B_p1, D_p1; /*!< Some auxiliar variables to improve readability of current wavefunction (1)*/

          //Asigning names to the Y vector
          for (int i = 0; i < 1; i++)
          {
            a1 = Y[i + 0];
            b1 = Y[i + 1];
            x01 = Y[i + 2];
            p01 = Y[i + 3];

            xa = Y[i + 4];
            xb = Y[i + 5];
            xx0 = Y[i + 6];
            xp0 = Y[i + 7];
          }

          // Calculating current electric field ////////////////////////
          if (for_back == 1) //For the forward propagation
          {
            Ex = E(t);
            //Ex=0;
          }
          else if (for_back == -1) //For the backward propagation. I only update the field for backward propagation
          {
            Ex = q * s(t, T, a0) * xp0 ;
          }
          else
          {
            throw invalid_argument("!!!!!!!!!!!!! for_back can only be 1 for forward or -1 for backward propagation !!!!!!!!!!!!!");
          }
          // Calculating current electric field ///////////////////////

          for (size_t i = 0; i < 1; i++) //Runs through all equations and assign the value of each parameter derivative \dot{Y_i}
          {

            //sumations for the propagation of the wavefunction
            double dv_dx01 = -q * E(t);
            double dv_da1 = 0;

            //sumations for the propagation of the Lagrange multipliers
            double dfb_da1 = -4 * a1 / m;
            double dfp0_da1 = 0;
            double dfb_dx01 = 0;
            double dfp0_dx01 = 0;

            //sumations for monitoring
            //pot = -q * x01 * E12;
            pot = 0;

            for (size_t p = 0; p < n_gauss; p++) //This for runs through every gaussian in the expanded external potential
            {
              //The derivative respect to alfa and x
              dv_da1 = dv_da1 + dV_da(a1, x01, g[p], b[p], xp[p]);
              dv_dx01 = dv_dx01 + dV_dx(a1, x01, g[p], b[p], xp[p]);

              //The sumations corresponding to the Lgrange multipliers
              dfb_da1 = dfb_da1 + dfb_da(a1,  x01, g[p], b[p], xp[p]);

              dfp0_da1 = dfp0_da1 + dfp0_da(a1,  x01, g[p], b[p], xp[p]);

              dfb_dx01 = dfb_dx01 + dfb_dx0(a1,  x01, g[p], b[p], xp[p]);

              dfp0_dx01 = dfp0_dx01 + dfp0_dx0(a1,  x01, g[p], b[p], xp[p]);

              //The potential
              pot = pot + V(a1, x01, g[p], b[p], xp[p]);
            }

            if (im_re == 1)
            {
              der[i + 0] = -2 * (pow(a1, 2) - pow(b1, 2)) / m - 4 * pow(a1, 2) * dv_da1; //Ecuation for imaginary propagation of alfa
              der[i + 1] = -(4 * a1 * b1) / m;                                           //Ecuation for imaginary propagation of beta

              der[i + 2] = (b1 * p01) / (a1 * m) - dv_dx01 / (2 * a1);                          //Ecuation for imaginary propagation of x0
              der[i + 3] = -2 * (pow(a1, 2) + pow(b1, 2)) * p01 / (a1 * m) - b1 * dv_dx01 / a1; //Ecuation for imaginary propagation of px0
            }

            else if (im_re == 2)
            {
              der[i + 0] = (4 * a1 * b1) / m;                                            //Ecuation for real propagation of alfa
              der[i + 1] = -2 * (pow(a1, 2) - pow(b1, 2)) / m - 4 * pow(a1, 2) * dv_da1; //Ecuation for real propagation of beta

              der[i + 2] = p01 / m;  //Ecuation for real propagation of x0
              der[i + 3] = -dv_dx01; //Ecuation for real propagation of p0

              //Equations for propagations of Lagrange Multipliers
              der[i + 4] = -(xa * 4 * b1) / m - xb * dfb_da1 - xp0 * dfp0_da1;
              der[i + 5] = -(xa * 4 * a1) / m - (xb * 4 * b1) / m;

              der[i + 6] = -xb * dfb_dx01 - xp0 * dfp0_dx01; 
              der[i + 7] = -xx0 / m;                         
            }
            // monit[0] = der[0];
            // monit[1] = der[1];
            // monit[2] = der[2];
            // monit[3] = der[3];
            // monit[4] = der[4];
            // monit[5] = der[5];
            // monit[6] = der[6];
            // monit[6] = der[7];
            // monit[6] = der[8];

            monit[0] = Ex;
            monit[1] = pot;
            monit[2] = x01;
            monit[3] = xx0;
            monit[4] = p01;
            monit[5] = xp0;
          }

          
      }

};
