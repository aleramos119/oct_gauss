import numpy as np
from scipy import integrate

from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
#%matplotlib qt
plt.style.use(['ggplot'])


import sys
#sys.path.append('/home/ar612/Documents/Python_modules/')
sys.path.append('/cluster/data/ar612/scripts/python/')
import mol

##### Defining functions ############################
def Vas(x,vmax,a,b):
    return (vmax/a**4)*(x**2-a**2)**2+b*x/a


def gauss(x,g,b,x0):
    return g*np.e**(-b*(x-x0)**2)

def x_gauss(x,g,b,x0):
    return x*g*np.e**(-b*(x-x0)**2)

def sum_gauss(x,g,b,x0):
    sum=0
    for i in range(0,len(g)):
        sum=sum + gauss(x,g[i],b[i],x0[i])
    return sum


def integ(f,xi,xf):
    i=integrate.quad(f,xi,xf)
    return i

######### Defining gaussians coeficients ####################
vmax=1
a=1

g=np.array([31.00001469*vmax,-1.52881858*vmax,-1.52881858*vmax,31.00001469*vmax,1.34845505*vmax])  ##/*!< Gaussian potential expansion height coefficients */
b=np.array([1.39712198*pow(a,-2),1.65840237*pow(a,-2),1.65840237*pow(a,-2),1.39712198*pow(a,-2),0.*pow(a,-2)]) ##     /*!<  Gaussian potential expansion wide coefficients */
xp=np.array([-2.98085171*a,-1.14213435*a,1.14213435*a,2.98085171*a,0.*a])      ##   /*!<  Gaussian potential expansion center coefficients */




######## Ploting the Potential ##################################
xmax=4
x=np.linspace(-xmax,xmax,1000)
#plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp))



#plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp),x, gauss(x,(2*alfa/np.pi)**(1/2.), 2*alfa , dat[3,0]) )





###%%
#%matplotlib qt
### Creating the subfigures
fig=plt.figure(1,figsize=(10,8))
plt.style.use(['ggplot'])


###Ploting the first figure
efield_file_pattern="results/t_monitor_$$.txt"
efield_file=efield_file_pattern.replace("$$",str(1))

te,et,v,over,x_phi,x_xi,p_phi,p_xi=mol.fast_load(efield_file,np.array([0,1,2,3,4,5,6,7]),1000).T


# spline_file_pattern="results/spline_$$.txt"
# spline_file=spline_file_pattern.replace("$$",str(0+1))
# tspline,espline=mol.fast_load(spline_file,np.array([0,1]),1000).T

fs2au=41.3414


te=te/fs2au
#tspline=tspline/fs2au

lim1=0.04
lim2=0.02
lim3=1
lim4=3
lim5=20
##%%
#%matplotlib qt
plt.subplot(231)
plt.axis([min(abs(te)), max(abs(te)), -lim1, lim1])
plt.plot(te,et, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("E (au)",fontweight='bold')
plt.legend()
plt.tick_params(labelsize=14)

plt.subplot(232)
plt.axis([min(abs(te)), max(abs(te)), 0, lim2])
plt.plot(te,v, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("V (au)",fontweight='bold')
plt.legend()
plt.tick_params(labelsize=14)

plt.subplot(233)
plt.axis([min(abs(te)), max(abs(te)), 0, lim3])
plt.plot(te,over, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("Overlap",fontweight='bold')
plt.legend()
plt.tick_params(labelsize=14)

plt.subplot(234)
plt.axis([min(abs(te)), max(abs(te)), -lim4, lim4])
plt.plot(te,x_phi, label="phi",color='b')
plt.plot(te,x_xi, label="xi",color='r')
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("x (au)",fontweight='bold')
plt.legend()
plt.tick_params(labelsize=14)

plt.subplot(235)
plt.axis([min(abs(te)), max(abs(te)), -lim5, lim5])
plt.plot(te,p_phi, label="phi",color='b')
plt.plot(te,p_xi, label="xi",color='r')
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("p (au)",fontweight='bold')
plt.legend()
plt.tick_params(labelsize=14)
##%%
imax=int(sys.argv[1])-1
## Set slide axes
axi_val = plt.axes([0.25, .00, 0.50, 0.02])
# Slider time
si_val = Slider(axi_val, 'i', 1, imax,valinit=1,valstep=1)


def update_i(val):

    i=int(si_val.val)

    efield_file_pattern="results/t_monitor_$$.txt"
    efield_file=efield_file_pattern.replace("$$",str(i))

    te,et,v,over,x_phi,x_xi,p_phi,p_xi=mol.fast_load(efield_file,np.array([0,1,2,3,4,5,6,7]),1000).T

    fs2au=41.3414

    te=te/fs2au


    plt.subplot(231)
    plt.cla()
    plt.axis([min(abs(te)), max(abs(te)), -lim1, lim1])
    plt.plot(te,et, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("E (au)",fontweight='bold')
    plt.legend()
    plt.tick_params(labelsize=14)

    plt.subplot(232)
    plt.cla()
    plt.axis([min(abs(te)), max(abs(te)), 0, lim2])
    plt.plot(te,v, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("V (au)",fontweight='bold')
    plt.legend()
    plt.tick_params(labelsize=14)

    plt.subplot(233)
    plt.cla()
    plt.axis([min(abs(te)), max(abs(te)), 0, lim3])
    plt.plot(te,over, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("Overlap",fontweight='bold')
    plt.legend()
    plt.tick_params(labelsize=14)

    plt.subplot(234)
    plt.cla()
    plt.axis([min(abs(te)), max(abs(te)), -lim4, lim4])
    plt.plot(te,x_phi, label="phi",color='b')
    plt.plot(te,x_xi, label="xi",color='r')
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("x (au)",fontweight='bold')
    plt.legend()
    plt.tick_params(labelsize=14)

    plt.subplot(235)
    plt.cla()
    plt.axis([min(abs(te)), max(abs(te)), -lim5, lim5])
    plt.plot(te,p_phi, label="phi",color='b')
    plt.plot(te,p_xi, label="xi",color='r')
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("p (au)",fontweight='bold')
    plt.legend()
    plt.tick_params(labelsize=14)


# call update function on slider value change
si_val.on_changed(update_i)

plt.show()
###%%
# import os
# os.getcwd()
#
# efield_file="results/t_Yt_1.txt"
# te,et=mol.fast_load(efield_file,np.array([0,3]),1000).T
#
# efield_file="results/t_Yt_1.txt"
# tspline,espline=mol.fast_load(efield_file,np.array([0,7]),1000).T
#
#
# efield_file="results/t_et_1.txt"
# te,et=mol.fast_load(efield_file,np.array([0,1]),1000).T
# #
# # efield_file="results/t_et_17.txt"
# # tspline,espline=mol.fast_load(efield_file,np.array([0,1]),1000).T
#
#
#
# plt.plot(te,et,'b-')
# plt.plot(tspline,espline,'b-')
